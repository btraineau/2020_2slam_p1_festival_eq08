<html>
    <head>
        <meta charset="UTF-8">
        <title>Représentation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        use modele\metier\Groupe;
        use modele\metier\Lieu;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Représentation</h2>";
        $unLieu= new Lieu('8', 'Lycée la Joliverie','141 route de Clisson', 10000);
        $unGroupe = new Groupe("g98", 'Groupe des claquettes anglaises', null, null, 23, 'Angleterre', 'N');
        $uneRepresentation = new Representation(1,$unLieu,$unGroupe, '20h20', '22h20', '2020-09-07');
        var_dump($uneRepresentation);
        ?>
    </body>
</html>
