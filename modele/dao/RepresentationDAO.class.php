<?php
namespace modele\dao;

use modele\metier\Representation;
use modele\metier\Groupe;
use modele\metier\Lieu;
use PDO;

/**
 * Description of RepresentationDAO
 * Classe métier :  Representation
 * @author eq08
 * @version 2020
 */
class RepresentationDAO {


    /**
     * Instancier un objet de la classe Representation à partir d'un enregistrement de la table REPRESENTATION
     * @param array $enreg
     * @return Representation
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $idLieu = $enreg['ID_LIEU'];
        $idGroupe = $enreg['ID_GROUPE'];
        $heureDebut = $enreg['HEURDEBUT'];
        $heureFin = $enreg['HEUREFIN'];
        $date = $enreg['DATEREP'];
        
        $objetLieu = LieuDAO::getOneById($idLieu);
        $objetGroupe = GroupeDAO::getOneById($idGroupe);
        $uneRepresentation = new Representation($id, $objetLieu, $objetGroupe, $heureDebut, $heureFin, $date);

        return $uneRepresentation;
    }
    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Groupe
     * @param Groupe $objetMetier un groupe
     * @param PDOStatement $stmt requête préparée
     */
   
    
    protected static function metierVersEnreg(Representation $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
         $groupe = $objetMetier->getGroupe();
        $lieu = $objetMetier->getLieu();
        
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':id_lieu', $lieu->getId());
        $stmt->bindValue(':id_groupe', $$groupe->getId());
        $stmt->bindValue(':heurdebut', $objetMetier->getHeureDebut());
        $stmt->bindValue(':heurefin', $objetMetier->getHeureFin());
        $stmt->bindValue(':daterep', $objetMetier->getDate());
    }

    
    protected static function metierVersEnregDate(array $enreg) {
        
        $date = $enreg["DATEREP"];
        return ($date);
        
    }
    /**
     * Retourne la liste de toutes les representations
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY DATEREP";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau groupe au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Recherche un groupe selon la valeur de son identifiant
     * @param string $id
     * @return Groupe le groupe trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }




    /**
     * Permet de vérifier s'il existe ou non un groupe ayant déjà le même identifiant dans la BD
     * @param string $id identifiant du groupe à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Groupe WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }
    
    public static function getAllDates() {
          $lesDates = array();
          $requete = "SELECT DATEREP FROM Representation";
          $stmt = Bdd::getPdo()->prepare($requete);
          $ok = $stmt->execute();
          if ($ok) {

              while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {

                  $lesDates[] = self::metierVersEnregDate($enreg);
              }
          }
          return $lesDates;
      }

}

