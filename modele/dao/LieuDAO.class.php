<?php

namespace modele\dao;
use modele\metier\Lieu;
use PDO;

/**
 * Description of DAOLieu
 *
 * @author alory@jolsio.net
 */
class LieuDAO {
       /**
     * Instancier un objet de la classe Groupe à partir d'un enregistrement de la table GROUPE
     * @param array $enreg
     * @return Groupe
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $nom = $enreg['NOM'];
        $adresse = $enreg['ADRESSE'];
        $capacite = $enreg['CAPACITE'];
        
        $unLieu = new Lieu($id, $nom, $adresse, $capacite);

        return $unLieu;
}

    
    
 
    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Groupe
     * @param Groupe $objetMetier un groupe
     * @param PDOStatement $stmt requête préparée
     */
    protected static function metierVersEnreg(Groupe $objetMetier, PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':nom', $objetMetier->getNom());
        $stmt->bindValue(':adresse', $objetMetier->getAdresse());
        $stmt->bindValue(':capacite', $objetMetier->getcapacite());
        
    }
    
    
    /**
     * Recherche un groupe selon la valeur de son identifiant
     * @param string $id
     * @return Groupe le groupe trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
    /**
     * Retourne la liste de tous les groupes
     * @return array tableau d'objets de type Groupe
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieu ORDER BY NOM";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau groupe au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }
    
    
    
    
}