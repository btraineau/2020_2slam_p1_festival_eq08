<?php
/**
 * Description Page de consultation des différentes représentations trié par date
 * @author eq08
 * @version 2020
 */

namespace vue\representations;

use vue\VueGenerique;
use modele\metier\Representation;
use modele\dao\RepresentationDAO;
use modele\metier\Groupe;
use modele\metier\Lieu;

class VueConsultationRepresentation extends VueGenerique {
    
    private $lesRepresentations;
    
    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        
        if (count($this->lesRepresentations) != 0) {
            $lesDates = RepresentationDAO::getAllDates();
            // pour chaque représentation : affichage du lieu, du groupe, de l'heure de début et de fin.
            if (is_array($lesDates)){
 
            foreach ($lesDates as $uneDate) {
                //affichage de la date d'une représentation
                ?>
            <strong><?= $uneDate ?></strong><br>
                <table width="45%" cellspacing="0" cellpadding="0" class="tabQuadrille">
                    <!--AFFICHAGE DE LA LIGNE D'EN-TÊTE-->
                    <tr class="enTeteTabQuad">
                        <td width="25%">Lieu</td>
                        <td width="25%">Groupe</td>
                        <td width="25%">Heures début</td> 
                        <td width="25%">Heures fin</td>
                    </tr>
                    <?php
                    foreach ($this->lesRepresentations as $uneRepresentation) { ?>
                        <tr class="ligneTabQuad">
                            <?php if(($uneRepresentation->getDate()) == $uneDate ){?> 
                            <td><?= $uneRepresentation->getUnLieu()->getNom() ?></td>
                            <td><?= $uneRepresentation->getUnGroupe()->getNom() ?></td>
                            <td><?= $uneRepresentation->getHeureDebut() ?></td>
                            <td><?= $uneRepresentation->getHeureFin() ?></td>  
                        <?php
                    }
                                }
                ?>
                </table><br>
                <?php
            
        }
    }
    include $this->getPied();
    }
    
    }
    public function setLesRepresentations(array $lesRepresentations) {
        $this->lesRepresentations = $lesRepresentations;
    }

    

    

}
